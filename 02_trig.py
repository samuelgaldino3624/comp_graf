from calendar import c
import pyglet
from math import sqrt

centro_x = 400
centro_y = 300
branco = (255,255,255)
amarelo = (255,255,0)
cinza = (50,50,50)
centro = pyglet.shapes.Circle(centro_x,centro_y,5, 
        color=branco)
ponto = pyglet.shapes.Circle(100,100,5,color=amarelo)
circulo = pyglet.shapes.Circle(centro_x, centro_y, 0, 
        segments=1000, color=cinza)
linha = pyglet.shapes.Line(0,0,0,0,2,color=amarelo)
texto = pyglet.text.Label("", x=20, y=20, font_size=20)

def dist(x1,y1,x2,y2):
    # pitágoras
    return sqrt((x1 - x2)**2 + (y1-y2)**2)

window = pyglet.window.Window(width=800,height=600)

@window.event
def on_draw():
    window.clear()
    circulo.draw()
    centro.draw()
    ponto.draw()
    linha.draw()
    texto.draw()
    

@window.event
def on_mouse_press(x, y, buttons, modifiers):
    ponto.position = (x,y)
    linha.position = (centro_x,centro_y,x,y)
    delta = dist(centro_x,centro_y,x,y)
    circulo.radius = delta
    texto.text = f"Distância entre os pontos {delta:.2f}"

pyglet.app.run()